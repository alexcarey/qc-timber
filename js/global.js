/* Custom JS */

jQuery(document).ready(function($) {

  $('.bxslider').bxSlider({
    auto: true,
    pager: false,
    pause: 8000,
    speed: 1000,
    onSliderLoad: function() {
      $(".bxslider-load").css("visibility", "visible");
    }
  });

  $('.desk-quote').bxSlider({
    slideWidth: 300,
    maxSlides: 4,
    moveSlides: 1,
    pause: 10000,
    speed: 1000,
    auto: true,
    pager: false,
    controls: false
  });

  $('.mob-quote').bxSlider({mode: 'horizontal', moveSlides: 1, auto: true, pager: false, controls: true, pause: 10000});

  $('.icon-search').click(function() {
		$(this).toggleClass('active');
		$('.doc_head__search').slideToggle();
		$("html, body").animate({ scrollTop: 0 }, "fast");
  });

  $('.js-search-trigger').click(function() {
    $(this).toggleClass('active');
    $('.js-search-content').slideToggle();
    $("html, body").animate({
      scrollTop: 0
    }, "fast");
  });
  $('.js-search-close').click(function() {
    $('.js-search-content').hide();
  });

  $('.js-hamburger_toggle').click(function() {
    $(this).toggleClass('active');
    $('.js-main_menu').toggleClass('active');
  });

  $('.accordion-faq-wrap').click(function() {
		$(this).toggleClass('active');
  });

  $('.menu-item-has-children').append( "<button class='submenu-trigger js-submenu-trigger'></button>" );


  $('.js-submenu-trigger').click(function() {
    $(this).toggleClass('active');
    $(this).parent().find('.main_menu__sub').toggleClass('active');
  });

  $(window).bind("scroll", function() {
    if ($(this).scrollTop() > 100) {
      $(".fixed-footer").fadeIn();
    } else {
      $(".fixed-footer").stop().fadeOut();
    }
  });
  $(function() {
    //caches a jQuery object containing the header element
    var header = $(".doc_head");
    $(window).scroll(function() {
        var scroll = $(window).scrollTop();

        if (scroll >= 900) {
            header.addClass("isScrolling");
        } else {
            header.removeClass("isScrolling");
        }
    });
});
  

});

/* Fade In */

$(document).ready(function() {
  var element = document.getElementById("js-fadeInElement");
  $(element).addClass('js-fade-element-hide');

  $(window).scroll(function() {
    if ($("#js-fadeInElement").length > 0) {
      var elementTopToPageTop = $(element).offset().top;
      var windowTopToPageTop = $(window).scrollTop();
      var windowInnerHeight = window.innerHeight;
      var elementTopToWindowTop = elementTopToPageTop - windowTopToPageTop;
      var elementTopToWindowBottom = windowInnerHeight - elementTopToWindowTop;
      var distanceFromBottomToAppear = 300;

      if (elementTopToWindowBottom > distanceFromBottomToAppear) {
        $(element).addClass('js-fade-element-show');
      } else if (elementTopToWindowBottom < 0) {
        $(element).removeClass('js-fade-element-show');
        $(element).addClass('js-fade-element-hide');
      }
    }
  });
});
