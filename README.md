
# The BC customized Timber Theme

## Installing the Theme

Be sure the Timber plugin is activated. But hey, let's break it down into some bullets:

1. Make sure you have installed the plugin for the [Timber Library](https://wordpress.org/plugins/timber-library/) (and Advanced Custom Fields Pro).
4. Activate the theme in Appearance >  Themes.

+ add custom post type = footer_quotes
