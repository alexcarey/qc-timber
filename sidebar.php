<?php
/**
 * The Template for the sidebar containing the main widget area
 *
 *
 * @package  WordPress
 * @subpackage  Timber
 */

 $context = array();
 $context['sidebar'] = Timber::get_widgets('sidebar_primary');
 Timber::render('sidebar.twig', $context);
