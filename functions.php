<?php

if ( ! class_exists( 'Timber' ) ) {
	add_action( 'admin_notices', function() {
		echo '<div class="error"><p>Timber not activated. Make sure you activate the plugin in <a href="' . esc_url( admin_url( 'plugins.php#timber' ) ) . '">' . esc_url( admin_url( 'plugins.php') ) . '</a></p></div>';
	});

	add_filter('template_include', function($template) {
		return get_stylesheet_directory() . '/static/no-timber.html';
	});

	return;
}

Timber::$dirname = array('templates', 'views');

class BicloneStarterSite extends TimberSite {

	function __construct() {
		add_theme_support( 'post-formats' );
		add_theme_support( 'post-thumbnails' );
		add_theme_support( 'menus' );
		add_theme_support( 'html5', array( 'comment-list', 'comment-form', 'search-form', 'gallery', 'caption' ) );
		add_filter( 'timber_context', array( $this, 'add_to_context' ) );
		add_filter( 'get_twig', array( $this, 'add_to_twig' ) );
		add_action( 'init', array( $this, 'register_post_types' ) );
		add_action( 'init', array( $this, 'register_taxonomies' ) );
		parent::__construct();
	}

	function register_post_types() {
		//this is where you can register custom post types
	}

	function register_taxonomies() {
		//this is where you can register custom taxonomies
	}

	function add_to_context( $context ) {
		$context['foo'] = 'bar';
		$context['stuff'] = 'I am a value set in your functions.php file';
		$context['notes'] = 'These values are available everytime you call Timber::get_context();';
		$context['menu'] = new TimberMenu( 'primary-navigation' );
		$context['menufooter'] = new TimberMenu( 'secondary-navigation' );
		$context['site'] = $this;
		$context['is_front_page'] = is_front_page();
		return $context;
	}

	function myfoo( $text ) {
		$text .= ' bar!';
		return $text;
	}

	function add_to_twig( $twig ) {
		/* this is where you can add your own functions to twig */
		$twig->addExtension( new Twig_Extension_StringLoader() );
		$twig->addFilter('myfoo', new Twig_SimpleFilter('myfoo', array($this, 'myfoo')));
		return $twig;
	}

}

new BicloneStarterSite();

// Load up google Fonts
function load_fonts() {
	wp_register_style('googleFonts', 'https://fonts.googleapis.com/css?family=Merriweather:300,400,400italic,700|Montserrat:400,700');
	wp_enqueue_style( 'googleFonts');
 }
add_action('wp_print_styles', 'load_fonts');

/* Google Analytics Code */
	
function ga_head() { ?>
	<script>
	  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
	
	  ga('create', 'UA-10783211-1', 'auto');
	  ga('send', 'pageview');
	
	</script>
	
	<?php
	}
	add_action( 'wp_head', 'ga_head' );

// Enqueue Scripts jQuery from Google http://css-tricks.com/snippets/wordpress/include-jquery-in-wordpress-theme/
if (!is_admin()) add_action("wp_enqueue_scripts", "custom_jquery_enqueue", 11);
function custom_jquery_enqueue() {
   wp_deregister_script('jquery');
   wp_register_script('jquery', "http" . ($_SERVER['SERVER_PORT'] == 443 ? "s" : "") . "://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js", false, null);
   wp_enqueue_script('jquery');
   wp_enqueue_script( 'site-js-minified', get_template_directory_uri() . '/js/app.min.js' );
}

// Add page slug to body class...helpful!

function add_slug_body_class( $classes ) {
	global $post;
	if ( isset( $post ) ) {
		$classes[] = $post->post_name;
	}
	return $classes;
}
add_filter( 'body_class', 'add_slug_body_class' );

/* We want to add a few custom styles to our Visual editor - http://alisothegeek.com/2011/05/tinymce-styles-dropdown-wordpress-visual-editor/ */

add_filter( 'mce_buttons_2', 'my_mce_buttons_2' );

function my_mce_buttons_2( $buttons ) {
    array_unshift( $buttons, 'styleselect' );
    return $buttons;
}

add_filter( 'tiny_mce_before_init', 'my_mce_before_init' );

function my_mce_before_init( $settings ) {

    $style_formats = array(
		array(
        	'title' => 'CTA Button',
        	'selector' => 'a',
    		'classes' => 'button'
        ),
		array(
        	'title' => 'Grey Italic Text',
        	'inline' => 'span',
    		'classes' => 'grey-em-text'
        )
    );

    $settings['style_formats'] = json_encode( $style_formats );

    return $settings;

}

/* Add ACF Options Page */

if( function_exists('acf_add_options_page') ) {

	acf_add_options_page(array(
		'page_title' 	=> 'Theme Options',
		'menu_title'	=> 'Theme Options'
	));

}

add_filter( 'timber_context', 'bitclone_timber_context'  );

function bitclone_timber_context( $context ) {
    $context['options'] = get_fields('option');
    return $context;
}
/**
 * Register widgetized areas.
 */
function bitclonetheme_widgets_init() {
	// Area 1,  Primary Widget Area.
	register_sidebar( array(
		'name' => __( 'Sidebar Primary', 'Bitclone' ),
		'id' => 'sidebar_primary',
		'description' => __( 'Sidebar Primary', 'Bitclone' ),
		'before_widget' => '<li id="%1$s" class="widget-container %2$s">',
		'after_widget' => '</li>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
	) );
}
/** Register sidebars by running bitclonetheme_widgets_init() on the widgets_init hook. */
add_action( 'widgets_init', 'bitclonetheme_widgets_init' );

add_filter( 'timber/context', 'add_to_context' );

function add_to_context( $context ) {

		$context['headernav'] = new \Timber\Menu( "Header Nav" );
		$context['footernav'] = new \Timber\Menu( "Footer Nav" );

    return $context;
}