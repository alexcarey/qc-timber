module.exports = function(grunt) {

// 1. All configuration goes here
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),

		copy: {
				 main: {
				   files: [
				   		{expand: true, cwd: 'bower_components/bxslider-4/', src: ['jquery.bxslider.min.js'], dest: 'js/vendor/'},
				   		{expand: true, cwd: 'bower_components/bxslider-4/', src: ['jquery.bxslider.css'], dest: 'js/vendor/css/'},
				   		{expand: true, cwd: 'bower_components/bxslider-4/images/', src: ['*'], dest: 'js/vendor/css/images/'},
				   		{expand: true, cwd: 'bower_components/waypoints/lib/', src: ['jquery.waypoints.js'], dest: 'js/vendor/'},
				   		{expand: true, cwd: 'bower_components/waypoints/lib/shortcuts/', src: ['infinite.js'], dest: 'js/vendor/'},
				   		{expand: true, cwd: 'bower_components/waypoints/lib/shortcuts/', src: ['inview.js'], dest: 'js/vendor/'},
				   		{expand: true, cwd: 'bower_components/waypoints/lib/shortcuts/', src: ['sticky.js'], dest: 'js/vendor/'}
				   ]
				 }
			},
    concat: {
			dist: {
				src: [
					// 'js/vendor/*.js', // Grab All JS in the vendor folder
					'js/vendor/jquery.bxslider.min.js', // Slider
          'js/vendor/jquery.waypoints.js', // Waypoints
          'js/vendor/sticky.js', // Waypoints Sticky
					'js/global.js'  // Site Specific
				],
				dest: 'js/app.js',
			}
    },
		sass: {
			dev: {
				options: {
					style: 'expanded',
					quiet: true // stop depreciation errors
				},
				files: {
					'stylesheets/app.css': 'stylesheets/app.scss'
				}
			},
			dist: {
				options: {
					style: 'compressed',
					quiet: true // stop depreciation errors
				},
				files: {
					'stylesheets/app.min.css': 'stylesheets/app.scss'
				}
			}
		},
		uglify: {
			build: {
				files: {
					'js/app.min.js': 'js/app.js',
				}
			}
		},
		watch: {
			//options: {
			//	livereload: true,
			//},
			scripts: {
			    files: ['js/*.js'],
			    tasks: ['concat', 'uglify'],
			    options: {
			        spawn: false,
				},
			},
			css: {
			    files: ['stylesheets/**/*.scss'],
			    tasks: ['sass', 'bsReload:css'],
			    options: {
			        spawn: false,
			    }
			}

		},
		browserSync: {
		    dev: {
                bsFiles: {
                    src : [
                        'stylesheets/app.scss',
                        '*.php',
                        'js/app.js',
                        'js/**/*.js',
                    ]
                },
		        options: {
		            watchTask: true,
		            proxy: 'quanta-change.localhost',
		            background: true
		        }
		    }
		},
        bsReload: {
            css: {
                reload: "style.css"
            },
            all: {
                reload: true
            }
        }
    });
// 3. Where we tell Grunt we plan to use this plug-in.
	grunt.loadNpmTasks('grunt-browser-sync');
	grunt.loadNpmTasks('grunt-contrib-copy');
	grunt.loadNpmTasks('grunt-contrib-concat');
	grunt.loadNpmTasks('grunt-contrib-uglify');
	grunt.loadNpmTasks('grunt-contrib-sass');
	grunt.loadNpmTasks('grunt-contrib-watch');
// 4. Where we tell Grunt what to do when we type "grunt" into the terminal.
	grunt.registerTask('default', ['copy', 'concat', 'sass', 'uglify', 'browserSync', 'watch']);

};
